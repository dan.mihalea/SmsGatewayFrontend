export const environment = {
  production: true,
  apiUrl: "http://danmihalea.go.ro/TikiTakaAPI/",
  defaultLanguage: "ro",
  hasMockData:false,
  mockUrl:"http://danmihalea.go.ro/TikiTaka/assets/mockData/"
};
