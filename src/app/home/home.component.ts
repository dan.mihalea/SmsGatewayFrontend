import { Component, Input, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { IMenuCategory } from '../interfaces/IMenuCategory';
import { MenuService } from '../services/menuService/menu.service';

@Component({
  selector: 'ap-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  @Input() title: string;

  categories: IMenuCategory[];

  constructor(private titleService: Title,
    private menuService: MenuService
    ) { }

  ngOnInit(): void {
    this.titleService.setTitle(this.title);
    this.menuService.categories()
      .subscribe(result=>this.categories = result)
  }

  private getPicture(categoryItem: IMenuCategory) {    
    return `./assets/pics/${categoryItem.image}`;
  }

}
