import { MatSnackBar } from '@angular/material/snack-bar';
import { MatIconModule } from '@angular/material/icon';
import { ListService } from './../../services/listService/list.service';
import { SmsService } from './../../services/smsService/sms.service';
import { ListsComponent } from './../../lists/lists.component';

import { DialogType } from './../../enums/dialogType';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { ContactsComponent } from 'src/app/contacts/contacts.component';
import { IContactItem } from 'src/app/interfaces/IContactItem';
import { ThrowStmt } from '@angular/compiler';


@Component({
  selector: 'ap-composer',
  templateUrl: './composer.component.html',
  styleUrls: ['./composer.component.css']
})
export class ComposerComponent implements OnInit {

  @ViewChild('messageContent') messageContent: ElementRef

  public dialogType = DialogType;
  selectedContacts: any[] = [];
  selectedLists: any[] = [];

  removable: true;
  isLoading = false;

  constructor(public dialog: MatDialog,
    private listService: ListService,
    private snackbar: MatSnackBar,
    private sms: SmsService) { }

  ngOnInit(): void {
  }
  ngAfterViewInit() {
  }

  openDialog(dialogType: DialogType) {
    switch (dialogType) {
      case DialogType.Contacts: {
        this.dialog.open(ContactsComponent, {
          width: '50%',
          data: { isSelection: true }
        })
          .afterClosed()
          .subscribe((result: Map<IContactItem, boolean>) => {
            this.selectedLists = [];
            if (this.selectedContacts)
              this.selectedContacts = Array.from(result.keys());
          });
        break;
      }
      case DialogType.List: {
        this.dialog.open(ListsComponent, {
          width: '50%',
          data: { isSelection: true }
        })
          .afterClosed()
          .subscribe(result => {
            this.selectedContacts = [];
            if (result) {
              this.selectedLists = result.map(x => x.value);
              this.listService.getLists()
            }
          });
        break;
      }
    }
  }

  removeContact(contact) {
    const index = this.selectedContacts.indexOf(contact);
    this.selectedContacts.splice(index, 1);
  }
  removeList(list) {
    const index = this.selectedLists.indexOf(list);
    this.selectedLists.splice(index, 1);
  }

  sendToContacts(contacts, text) {
    this.isLoading = true;
    this.sms.login().subscribe(
      response => {
        this.sms.sendMessageToContacts(contacts, text)
          .subscribe(response => {
            this.isLoading = false;
            this.messageContent.nativeElement.value = ''
            this.snackbar.open("Message was sent", "Close")
          },
            error => {
              console.error(error);
              this.isLoading = false;
              this.snackbar.open("Something went wrong", "Close")
            })
      },
      error => {
        console.error(error);
        this.isLoading = false;
        this.snackbar.open("Something went wrong", "Close")
      }
    );

  }

  sendToLists(lists, text) {
    this.isLoading = true;
    this.sms.login().subscribe(
      response => {
        this.sms.sendMessageToLists(lists, text)
          .subscribe(response => {
            this.isLoading = false;
            this.messageContent.nativeElement.value = '',
              this.snackbar.open("Message was sent", "Close")
          },
            error => {
              console.error(error);
              this.isLoading = false;
              this.snackbar.open("Something went wrong", "Close")
            })
      },
      error => {
        console.error(error);
        this.isLoading = false;
        this.snackbar.open("Something went wrong", "Close")
      }
    );

  }


}
