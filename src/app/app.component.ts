import { Component } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer, Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(router: Router, activatedRoute: ActivatedRoute,
    private title: Title,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer
  ) {
    this.addIcon('ro', './assets/flags/ro.svg');
    this.addIcon('en', './assets/flags/en.svg');
  }

  addIcon(iconName, iconPath) {
    this.matIconRegistry.addSvgIcon(iconName, this.domSanitizer.bypassSecurityTrustResourceUrl(iconPath));
  }

  getDepth(outlet) {
    this.title.setTitle(outlet.activatedRouteData['title']);
    return outlet.activatedRouteData['depth'];
  }
}


