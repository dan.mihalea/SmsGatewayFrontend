import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'roPhone'
})
export class RoPhonePipe implements PipeTransform {

  transform(value: string, args?: any) {
    if (!value)
      return null;

    let hasInternationalCode = value.startsWith('+') ? true : false;

    if (hasInternationalCode) {
      let intCode = value.substring(0, 2);
      let operatorCode = value.substring(2, 6);
      let firstGroup = value.substring(6, 9);
      let secondaryGroup = value.substring(9, 12);

      return `${intCode} ${operatorCode}.${firstGroup}.${secondaryGroup}`;
    }
    else {
      //let intCode = value.substring(0, 2);
      let operatorCode = value.substring(0, 4);
      let firstGroup = value.substring(4, 7);
      let secondaryGroup = value.substring(7, 10);

      return `${operatorCode}.${firstGroup}.${secondaryGroup}`;
    }

    //return value;


  }

}
