export enum DialogType {
    'Contacts' = 0,
    'List' = 1,
    'AddContact' = 2
}