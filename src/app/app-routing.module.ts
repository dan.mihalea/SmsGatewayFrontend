import { PizzaComponent } from './menu/pizza/pizza.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContactsComponent } from './contacts/contacts.component';
import { HomeComponent } from './home/home.component';
import { MessengerComponent } from './messaging/messenger/messenger.component';
import { ListsComponent } from './lists/lists.component';
import { DrinksComponent } from './menu/drinks/drinks/drinks.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'contacts', component: ContactsComponent },
  { path: 'pizza', component: PizzaComponent },
  { path: 'messaging', component: MessengerComponent },
  { path: 'contacts', component: ContactsComponent },
  { path: 'lists', component: ListsComponent },
  { path: 'drinks', component: DrinksComponent },
  { path: '', component: HomeComponent },
  { path: '**', component: HomeComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
