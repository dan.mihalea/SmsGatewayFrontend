import { LanguageService } from './services/language.service';

import { MAT_DIALOG_DATA } from '@angular/material/dialog';
// core libraries
import { BrowserModule, Title } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule } from '@angular/common/http';

// services
import { EventService } from './services/eventService/event.service';
import { ListService } from './services/listService/list.service';
import { ContactService } from './services/contactService/contact.service';
import { SmsService } from './services/smsService/sms.service';
import { MenuService } from './services/menuService/menu.service';

// angular material modules
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatTabsModule } from '@angular/material/tabs';
import { MatDialogModule } from '@angular/material/dialog';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatMenuModule } from '@angular/material/menu';
import { MatChipsModule } from '@angular/material/chips';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatSelectModule } from '@angular/material/select';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatBadgeModule } from '@angular/material/badge';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatStepperModule } from '@angular/material/stepper';
import { MatExpansionModule } from '@angular/material/expansion';
// componenets
import { ContactsComponent } from './contacts/contacts.component';
import { HomeComponent } from './home/home.component';
import { AppComponent } from './app.component';
import { MainNavComponent } from './main-nav/main-nav.component';
import { SmallCardComponent } from './cards/small-card.component';
import { ContactSearchComponent } from './contacts/contact-search/contact-search.component';
import { ContactListComponent } from './contacts/contact-list/contact-list.component';
import { PizzaComponent } from './menu/pizza/pizza.component';
import { MessengerComponent } from './messaging/messenger/messenger.component';
import { ComposerComponent } from './messaging/composer/composer.component';
import { SmsContactsDialogComponent } from './dialogs/sms-contacts-dialog/sms-contacts-dialog.component';
import { SmsListDialogComponent } from './dialogs/sms-list-dialog/sms-list-dialog.component';
import { ListsComponent } from './lists/lists.component';
import { AddListContactComponent } from './dialogs/add-list-contact/add-list-contact.component';
import { ConfirmationComponent } from './dialogs/confirmation/confirmation.component';
import { DrinksComponent } from './menu/drinks/drinks/drinks.component';

//3rd party module imports
import { TableVirtualScrollModule } from 'ng-table-virtual-scroll';
import { CdkColumnDef } from '@angular/cdk/table';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

//pipes
import { RoPhonePipe } from './pipes/intl-phone.pipe';
import { environment } from 'src/environments/environment';
import { ServiceWorkerModule } from '@angular/service-worker';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/translations/', '.json');
}
@NgModule({
  declarations: [
    AppComponent,
    MainNavComponent,
    ContactsComponent,
    HomeComponent,
    SmallCardComponent,
    ContactSearchComponent,
    ContactListComponent,
    PizzaComponent,
    MessengerComponent,
    ComposerComponent,
    SmsContactsDialogComponent,
    SmsListDialogComponent,
    ListsComponent,
    AddListContactComponent,
    ConfirmationComponent,
    DrinksComponent,

    RoPhonePipe,

    DrinksComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,

    // angular material modules
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatTooltipModule,
    MatTabsModule,
    MatDialogModule,
    MatCheckboxModule,
    ScrollingModule,
    MatMenuModule,
    MatChipsModule,
    MatGridListModule,
    MatSelectModule,
    MatProgressBarModule,
    MatBadgeModule,
    MatSnackBarModule,
    MatStepperModule,
    MatGridListModule,
    MatExpansionModule,

    //3rd party modules
    TableVirtualScrollModule,

    //ngx-translate
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),

    ServiceWorkerModule.register('./ngsw-worker.js', {
      enabled: environment.production,
    }),
  ],
  providers: [
    EventService,
    ContactService,
    SmsService,
    ListService,
    LanguageService,
    MenuService,

    CdkColumnDef,
    Title,
    { provide: MAT_DIALOG_DATA, useValue: {} },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
