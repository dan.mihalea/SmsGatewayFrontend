import { LanguageService } from './../services/language.service';
import { Component, Input, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { Title } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';
import { MenuService } from '../services/menuService/menu.service';
import { IMenuCategory } from '../interfaces/IMenuCategory';

@Component({
  selector: 'ap-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.css']
})
export class MainNavComponent implements OnInit {
  appTitle: string;
  allLanguages = [];
  availableLanguages = [];
  selectedLanguage: any;
  categories:IMenuCategory[];
  

  @Input() title: string;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver,
    private translate: TranslateService,
    private languages: LanguageService,
    private menuService: MenuService,
    private titleService: Title) {

  }

  ngOnInit(): void {
    this.appTitle = this.title;
    this.titleService.setTitle(this.title);
    this.allLanguages = this.languages.getAvailableLanguages();
    this.setDefaultLanguage();
    this.getMenuCategories();

  }

  setLanguage(language) {
    let [first] = this.allLanguages.filter(l => l.key == language);
    this.selectedLanguage = first;
    this.availableLanguages = this.allLanguages.filter(l => l.key !== this.selectedLanguage.key)
    this.translate.use(language);
  }

  setDefaultLanguage() {
    this.setLanguage(this.allLanguages
      .filter(l => l.key == environment.defaultLanguage)
      .map(l => l.key));
  }

  getMenuCategories() {
    this.menuService.categories()
      .subscribe(result=>this.categories = result)
  }

}
