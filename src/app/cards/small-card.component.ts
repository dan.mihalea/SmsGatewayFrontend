import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'ap-small-card',
  templateUrl: './small-card.component.html',
  styleUrls: ['./small-card.component.css']
})
export class SmallCardComponent {

  @Input() title: string;
  @Input() subtitle: string;
  @Input() selectedImage: string;
  @Input() description: string;
  @Input() price: number;


  isSelected = false;
  constructor() { }

  toggleSelected() {
    this.isSelected = !this.isSelected;
  }

}
