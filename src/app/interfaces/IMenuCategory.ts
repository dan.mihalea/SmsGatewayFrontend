import { IMenuItem } from "./IMenuItem";

export interface IMenuCategory {
    name: string,
    image: string
}