export interface IIngredient {
    name: string,
    quantity: number,
    unit: string
}