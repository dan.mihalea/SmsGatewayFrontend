export interface IContactItem {
    id: number;
    contactName: string;
    phoneNumber: string;
    lists: string[];
}