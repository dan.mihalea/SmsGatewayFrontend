import { IIngredient } from "./IIngredient";
import { IPrice } from "./IPrice";

export interface IMenuItem {
    name: string,
    description:string,
    itemPrice: IPrice,
    quantity: number,
    unit: string,
    ingredients: IIngredient[]
}
