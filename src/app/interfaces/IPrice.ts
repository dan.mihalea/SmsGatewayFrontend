import { ICurrency } from "./ICurrency";

export interface IPrice {
    value: number,
    currency: ICurrency
}