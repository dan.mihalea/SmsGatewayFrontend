import { BaseService } from 'src/app/services/base.service';
import { IContactItem } from './../../interfaces/IContactItem';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SmsService extends BaseService {
  constructor(private http: HttpClient) {
    super();
  }

  login() {
    let postData = {
      Username: "admin",
      Password: btoa("admin")
    }
    return this.http.post(`${this.url}api/user/login`, postData, this.httpOptions);
  }

  sendMessageToContacts(contacts: IContactItem[], text: string) {

    let requestData = {
      Phones: contacts.map(item => item.phoneNumber),
      Content: text
    }
    return this.http.post(`${this.url}api/sms/send-sms`, requestData, this.httpOptions);
  }

  sendMessageToLists(lists, text) {
    let requestData = {
      listIds: lists.map(x => x.id),
      message: text
    }
    console.log(requestData);
    return this.http.post(`${this.url}api/sms/list-sms`, requestData, this.httpOptions);
  }
}
