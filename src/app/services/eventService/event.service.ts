import { IContactItem } from 'src/app/interfaces/IContactItem';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class EventService {

  private messageSource = new BehaviorSubject<string>("");
  currentMessage = this.messageSource.asObservable();

  private contactSelection = new BehaviorSubject<any>({});
  currentSelection = this.contactSelection.asObservable();

  private tableRefresh = new BehaviorSubject<any>({});
  currentRefresh = this.tableRefresh.asObservable();

  constructor() { }

  changeMessage(message: string) {
    this.messageSource.next(message);
  }

  updateSelection(selection: any) {
    this.contactSelection.next(selection);
  }

  refreshTableData(shouldRefreshWith: any) {
    this.tableRefresh.next(shouldRefreshWith);
  }
}
