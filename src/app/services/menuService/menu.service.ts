import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseService } from '../base.service';

@Injectable({
  providedIn: 'root'
})
export class MenuService extends BaseService {

  constructor(private http: HttpClient) {
    super();
   }

  categories(): Observable<any> {
    return this.http.get(`${this.getResource("categories")}`)
    //return this.http.get(`${this.url}api/menu/categories`);
  }
  drinks(): Observable<any> {
    return this.http.get(`${this.getResource("drinks")}`)
  }
}
