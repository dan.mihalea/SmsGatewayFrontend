import { environment } from 'src/environments/environment';
import { HttpHeaders } from '@angular/common/http';

export class BaseService {
  url: string;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Accept: 'application/json',
    }),
  };

  hasMockData: boolean;
  path: string;

  constructor() {
    this.hasMockData = environment.hasMockData;
    this.path = this.hasMockData
      ? '../../assets/mockData/'
      : '../../assets/mockData/';
    this.url = this.hasMockData ? this.path : environment.mockUrl;
  }

  protected getResource(resource: string): string {
    return `${this.url}${resource}.json`;
  }
}
