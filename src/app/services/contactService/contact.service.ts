import { MatIconModule } from '@angular/material/icon';
import { EMPTY, Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from 'src/app/services/base.service';

@Injectable({
  providedIn: 'root'
})
export class ContactService extends BaseService {

  constructor(private http: HttpClient) {
    super();
  }
  getApiContacts() {
    return this.http.get(`${this.url}api/contact`);
  }

  addContact(contact) {
    let contactData = {
      contactName: contact.contactName,
      phoneNumber: contact.phoneNumber,
      lists: contact.lists ? [{ listId: contact.lists.id }] : []
    }
    let postData = JSON.stringify(contactData);
    return this.http.post(`${this.url}api/contact`, postData, this.httpOptions);
  }

  removeContact(contacts: any[]): Observable<any> {
    let postData = JSON.stringify(contacts);
    return this.http.post(`${this.url}api/contact/remove`, postData, this.httpOptions);
  }

  addContactsToList(listId, selectedContacts) {
    let contactData = [...selectedContacts.keys()];
    let postData = [];
    contactData.forEach(item => postData.push(item.id));
    return this.http.post(`${this.url}api/list/${listId}/addUsers`, postData, this.httpOptions);
  }

}


