import { Observable } from 'rxjs';
import { BaseService } from './../base.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ListService extends BaseService {

  constructor(
    private http: HttpClient
  ) {
    super();
  }

  getLists(): Observable<any> {
    return this.http.get(`${this.url}api/list`);
  }

  getList(listId): Observable<any> {
    return this.http.get(`${this.url}api/list/${listId}`);
  }

  addList(list) {
    let listData = {
      listName: list.listName,
      created: new Date().toISOString()
    }
    let postData = JSON.stringify(listData);
    return this.http.post(`${this.url}api/list`, postData, this.httpOptions);
  }

  removeLists(lists) {
    let listIds = lists.map(list => list.value.id);
    let postData = JSON.stringify(listIds);
    return this.http.post(`${this.url}api/list/remove`, postData, this.httpOptions);
  }
}
