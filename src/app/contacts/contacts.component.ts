import { EventService } from './../services/eventService/event.service';
import { Component, Inject, Input, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'ap-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css']
})
export class ContactsComponent implements OnInit {
  @Input() isSelection: boolean = false;
  @Input() isList: boolean = false;

  constructor(@Inject(MAT_DIALOG_DATA) data: any,
    private eventService: EventService
  ) {
    this.isSelection = data.isSelection || this.isSelection;
    this.isList = data.isList || this.isList;
  }

  ngOnInit(): void {
    this.eventService.updateSelection({});
  }

}
