import { MatSnackBar } from '@angular/material/snack-bar';
import { IContactItem } from 'src/app/interfaces/IContactItem';
import { MatTableDataSource } from '@angular/material/table';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { EventService } from 'src/app/services/eventService/event.service';
import { ContactService } from 'src/app/services/contactService/contact.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';




@Component({
  selector: 'ap-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.css']
})
export class ContactListComponent implements OnInit {

  @Input() isSelection;
  @Input() isList;

  isLoading = false;
  allSelected = false;
  receivedContacts: IContactItem[] = [];
  contactList = new MatTableDataSource<IContactItem>(this.receivedContacts);

  activeColumns: string[] = ['contactName', 'phoneNumber', 'lists', 'Actions'];
  contactSelection = new Map<IContactItem, boolean>();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private eventService: EventService,
    private contactService: ContactService,
    private snackBar: MatSnackBar,
  ) { }

  ngOnInit(): void {

    this.eventService.currentRefresh
      .subscribe(() => {
        this.refreshTable();
      });

    this.eventService.currentMessage
      .subscribe(message => this.contactList.filter = message.trim().toLowerCase());
  }

  ngAfterViewInit() {
    this.contactList.paginator = this.paginator;
    this.contactList.sort = this.sort;
  }

  onSelectAll($event) {
    if ($event.checked) {
      this.contactList.data.forEach((item) => {
        this.contactSelection.set(item, $event.checked);
      });
      this.allSelected = true;
    }
    else {
      this.contactList.data.forEach((item) => {
        this.contactSelection.delete(item);
      })
      this.allSelected = false;
    }
    this.eventService.updateSelection(this.contactSelection);
  }

  someSelected() {
    return this.contactSelection.size !== this.contactList.data.length && this.contactSelection.size > 0;
  }

  onChange($event) {
    if ($event.checked) {
      this.contactSelection.set($event.source.value, $event.checked);
      this.allSelected = this.contactList.data.length == this.contactSelection.size;
    }
    else {
      this.contactSelection.delete($event.source.value);
      this.allSelected = false;
    }
    this.eventService.updateSelection(this.contactSelection);
  }

  hasContact(contact) {
    return this.contactSelection.has(contact) ? this.contactSelection.get(contact).valueOf() : false;
  }

  private refreshTable() {
    this.isLoading = true;
    this.contactService.getApiContacts()
      .subscribe((response: any[]) => {
        this.contactList.data = response;
        this.isLoading = false;
      }, error => {
        this.isLoading = false;
        console.error(error);
        this.snackBar.open("Something went wrong...", "Close")
      });
  }

}
