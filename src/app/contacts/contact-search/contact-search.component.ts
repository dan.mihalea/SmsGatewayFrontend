import { MatSnackBar } from '@angular/material/snack-bar';
import { ConfirmationComponent } from './../../dialogs/confirmation/confirmation.component';
import { AddListContactComponent } from './../../dialogs/add-list-contact/add-list-contact.component';
import { Component, Inject, Input, OnInit, } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { SmsContactsDialogComponent } from 'src/app/dialogs/sms-contacts-dialog/sms-contacts-dialog.component';
import { IContactItem } from 'src/app/interfaces/IContactItem';
import { EventService } from 'src/app/services/eventService/event.service';

import { ContactService } from 'src/app/services/contactService/contact.service';


@Component({
  selector: 'ap-contact-search',
  templateUrl: './contact-search.component.html',
  styleUrls: ['./contact-search.component.css']
})
export class ContactSearchComponent implements OnInit {

  @Input() isSelection: boolean;
  @Input() isList: boolean;

  selectionCount: number = 0;
  selectedContacts: any;

  constructor(private eventService: EventService,
    private contactService: ContactService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.eventService.currentSelection
      .subscribe((selection: Map<IContactItem, boolean>) => {
        this.selectionCount = selection.size;
        this.selectedContacts = selection;
      });
  }
  applyFilter(event: Event) {
    this.eventService.changeMessage((event.target as HTMLInputElement).value);
  }

  addContactDialog() {
    this.dialog.open(SmsContactsDialogComponent, {
      width: '30%',
      data: { id: 0 }
    })
      .afterClosed()
      .subscribe(result => {
        if (result) {
          this.contactService.addContact(result.value)
            .subscribe(result => {
              this.eventService.refreshTableData(true)
            }, error => {
              console.error(error)
              this.snackBar.open(error.error.errorMessage, "Close");
            });
        }
      });
  }

  addListContactDialog() {
    this.dialog.open(AddListContactComponent, {
      width: '30%',
      data: { contacts: this.selectedContacts }
    })
      .afterClosed()
      .subscribe(result => {
        this.contactService.addContactsToList(result.value, this.selectedContacts)
          .subscribe(result => this.eventService.refreshTableData(true));
      })
  }

  removeContacts(contacts: Map<IContactItem, boolean>) {
    let contactIds = [];
    for (let item of contacts.keys()) {
      contactIds.push(item.id);
    }
    this.contactService.removeContact(contactIds)
      .subscribe(result => {
        this.eventService.refreshTableData(true);
      }, error => {
        console.log(error);

      });
  }

  confirm(action) {
    this.dialog.open(ConfirmationComponent, {
      data: { dialogAction: action, objects: 'contacts' }
    })
      .afterClosed()
      .subscribe(result => {
        if (result) {
          this.removeContacts(this.selectedContacts);
        }
      })
  }


}
