import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ap-pizza',
  templateUrl: './pizza.component.html',
  styleUrls: ['./pizza.component.css']
})
export class PizzaComponent implements OnInit {

  pizzas: any[] = [
    {
      "picture": "./assets/pics/pizza-carbonara.jpg",
      "price": 33,
      "name": "Pizza Carbonara",
      "shortDesc": "World famous Carbonara",
      "description": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempora, officiis!"
    },
    {
      "picture": "./assets/pics/pizza-classic.jpg",
      "price": 48,
      "name": "Classic pizza",
      "shortDesc": "The classic taste of pizza",
      "description": "500 gr. carnati, bacon, ciuperci, ardei gras, porumb, ceapa, mozzarella, sos rosii, aluat"
    },
    {
      "picture": "./assets/pics/pizza-country.jpg",
      "price": 30,
      "name": "Country pizza",
      "shortDesc": "Straight from the countryside",
      "description": "Lorem ipsum dolor sit amet consectetur adipisicing elit."
    },
    {
      "picture": "./assets/pics/pizza-rustic.jpg",
      "price": 31,
      "name": "Pizza rustic",
      "shortDesc": "The traditional one",
      "description": "Lorem ipsum dolor sit amet consectetur adipisicing elit, libero animi!"
    },
    {
      "picture": "./assets/pics/pizza-carnivor.jpg",
      "price": 19,
      "name": "Carnivorous pizza",
      "shortDesc": "Meat packed pizza",
      "description": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur dolore dolorum quas eius, libero animi!"
    },
    {
      "picture": "./assets/pics/pizza-bbq.jpg",
      "price": 34,
      "name": "BBQ Pizza",
      "shortDesc": "The American way",
      "description": "Lorem ipsum dolor sit amet consectetur adipisicing elit, libero animi!"
    }
  ]

  constructor() { }

  ngOnInit(): void {
  }

}
