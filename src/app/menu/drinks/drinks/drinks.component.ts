import { Component, OnInit } from '@angular/core';
import { IMenuCategory } from 'src/app/interfaces/IMenuCategory';
import { MenuService } from 'src/app/services/menuService/menu.service';

@Component({
  selector: 'ap-drinks',
  templateUrl: './drinks.component.html',
  styleUrls: ['./drinks.component.css']
})
export class DrinksComponent implements OnInit {
  withCards:boolean = true
  menuData: IMenuCategory[]
  imagePath="https://via.placeholder.com/100"  

  constructor(private menuService: MenuService
    ) { }

  ngOnInit(): void {
    this.menuService.drinks()
      .subscribe(result => this.menuData =result)
  }
}
