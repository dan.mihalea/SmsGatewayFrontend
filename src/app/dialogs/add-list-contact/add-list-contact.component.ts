import { FormControl } from '@angular/forms';
import { ListService } from 'src/app/services/listService/list.service';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, Inject, OnInit } from '@angular/core';

@Component({
  selector: 'ap-add-list-contact',
  templateUrl: './add-list-contact.component.html',
  styleUrls: ['./add-list-contact.component.css']
})
export class AddListContactComponent implements OnInit {

  selectedContacts;
  selectedList = new FormControl();
  lists;



  constructor(@Inject(MAT_DIALOG_DATA) data: any,
    private listService: ListService
  ) {
    this.selectedContacts = [...data.contacts];

  }

  ngOnInit(): void {
    this.listService.getLists()
      .subscribe(response => this.lists = response)
  }

}
