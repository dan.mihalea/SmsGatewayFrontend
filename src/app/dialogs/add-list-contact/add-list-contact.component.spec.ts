import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddListContactComponent } from './add-list-contact.component';

describe('AddListContactComponent', () => {
  let component: AddListContactComponent;
  let fixture: ComponentFixture<AddListContactComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddListContactComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddListContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
