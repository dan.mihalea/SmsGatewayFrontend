import { ListService } from './../../services/listService/list.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, Inject, OnInit } from '@angular/core';
import { DialogType } from 'src/app/enums/dialogType';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { map } from 'rxjs/operators';

@Component({
  selector: 'ap-sms-contacts-dialog',
  templateUrl: './sms-contacts-dialog.component.html',
  styleUrls: ['./sms-contacts-dialog.component.css']
})
export class SmsContactsDialogComponent implements OnInit {

  lists: any[] = [];
  isNewContact: boolean;

  constructor(@Inject(MAT_DIALOG_DATA) data: any,
    private dialogRef: MatDialogRef<SmsContactsDialogComponent>,
    private listService: ListService
  ) {
    this.isNewContact = data.id == 0;
  }

  ngOnInit(): void {
    this.listService.getLists()
      .subscribe(response => this.lists = response)
  }

  addContact: FormGroup = new FormGroup({
    id: new FormControl(0),
    contactName: new FormControl('', Validators.required),
    phoneNumber: new FormControl('', Validators.required),
    lists: new FormControl()
  });

  noClick() {
    this.dialogRef.close();
  }

}
