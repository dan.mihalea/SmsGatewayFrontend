import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SmsContactsDialogComponent } from './sms-contacts-dialog.component';

describe('SmsContactsDialogComponent', () => {
  let component: SmsContactsDialogComponent;
  let fixture: ComponentFixture<SmsContactsDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SmsContactsDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SmsContactsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
