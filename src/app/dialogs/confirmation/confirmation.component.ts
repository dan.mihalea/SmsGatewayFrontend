import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, Inject, OnInit } from '@angular/core';

@Component({
  selector: 'ap-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.css']
})
export class ConfirmationComponent implements OnInit {

  dialogAction: string;
  objects: string;
  constructor(
    @Inject(MAT_DIALOG_DATA) data: any
  ) {
    this.dialogAction = data.dialogAction;
    this.objects = data.objects
  }

  ngOnInit(): void {
  }

}
