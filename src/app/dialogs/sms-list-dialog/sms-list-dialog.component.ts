import { FormGroup, FormControl } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { ContactsComponent } from './../../contacts/contacts.component';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'ap-sms-list-dialog',
  templateUrl: './sms-list-dialog.component.html',
  styleUrls: ['./sms-list-dialog.component.css']
})
export class SmsListDialogComponent implements OnInit {

  @ViewChild('contacts') contacts: ContactsComponent;

  listForm = new FormGroup({
    listName: new FormControl(''),
    contacts: new FormControl([])
  });

  constructor(
    private dialogRef: MatDialogRef<SmsListDialogComponent>
  ) { }

  ngOnInit(): void {

  }

  onClose() {
    this.dialogRef.close();
  }


}
