import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SmsListDialogComponent } from './sms-list-dialog.component';

describe('SmsListDialogComponent', () => {
  let component: SmsListDialogComponent;
  let fixture: ComponentFixture<SmsListDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SmsListDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SmsListDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
