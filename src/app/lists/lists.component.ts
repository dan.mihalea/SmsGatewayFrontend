import { ConfirmationComponent } from './../dialogs/confirmation/confirmation.component';
import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { SmsListDialogComponent } from 'src/app/dialogs/sms-list-dialog/sms-list-dialog.component';
import { ListService } from 'src/app/services/listService/list.service';
import { MatSelectionList } from '@angular/material/list';

@Component({
  selector: 'ap-lists',
  templateUrl: './lists.component.html',
  styleUrls: ['./lists.component.css']
})
export class ListsComponent implements OnInit {

  lists = [];
  visibleData = [];

  isSelection: boolean;
  isLoading: boolean;
  selectionCount;

  paginatorPageSize = 10;
  paginatorPageSizes = [5, 10, 25];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSelectionList) contactLists: MatSelectionList;

  constructor(@Inject(MAT_DIALOG_DATA) data: any,
    private dialog: MatDialog,
    private listService: ListService
  ) {
    this.isSelection = data.isSelection;
  }

  ngOnInit(): void {
    this.fetchData();
  }

  private fetchData() {
    this.isLoading = true;
    this.listService.getLists()
      .subscribe(response => {
        this.lists = response;
        this.visibleData = this.lists.slice(0, this.paginatorPageSize);
        this.isLoading = false;
      }, error => { console.error(error); this.isLoading = false });
  }

  applyFilter(event) {
    const filterValue = (event.target as HTMLInputElement).value;
    console.log(filterValue);
    this.visibleData = this.lists.filter(list => {
      return list.listName.indexOf(filterValue.trim().toLowerCase()) > -1;
    });
  }

  onPageChange(e) {
    let firstCut = e.pageIndex * e.pageSize;
    let secondCut = firstCut + e.pageSize;
    this.visibleData = this.lists.slice(firstCut, secondCut);
  }

  openDialog() {
    this.dialog.open(SmsListDialogComponent, {
      width: '20%',
      data: { isSelection: true, isList: true }
    })
      .afterClosed()
      .subscribe(result => {
        if (result) {
          this.listService.addList(result.value)
            .subscribe(response => {
              this.fetchData();
            });
        }
      });
  }

  remove(lists) {
    this.listService.removeLists(lists)
      .subscribe(response => this.fetchData());

  }

  confirm(action) {
    this.dialog.open(ConfirmationComponent, {
      data: { dialogAction: action, objects: 'lists' }
    })
      .afterClosed()
      .subscribe(result => {
        if (result) {
          this.remove(this.contactLists.selectedOptions.selected)
        }
      })
  }

}
